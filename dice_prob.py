from graph import plot_data
from damage_table import get_damage

# Check for result of a normal roll of 2d6
dice_data = []
for i in range(1, 7):
    for j in range(1, 7):
        dice_data.append(i + j)
dice_data = sorted(dice_data)

plot_data([dice_data], label_x="Dice sum", title="simple_test_probs", labels=[])

# Check magical resistance
dice_data = [[] for _ in range(6, 13)]
for resistance in range(6, 13):
    for i in range(1, 7):
        for j in range(1, 7):
            for op_i in range(1, 7):
                for op_j in range(1, 7):
                    test = 12 - i - j
                    if test <= 0 or i + j == 12:
                        dice_data[resistance-6].append(0)
                    else:
                        test_op = resistance - op_i - op_j - test
                        if test_op <= 0 or op_i + op_j == 12:
                            dice_data[resistance-6].append(1)
                        else:
                            dice_data[resistance-6].append(0)
plot_data(dice_data, label_x="Blinded or not", title="blindness_resistance",
          labels=["Res %d" % x for x in range(6, 12)])


# Check for crossbow roll depending of the MR
CROSSBOW_TEST = 11
VANILLA_LABEL = "Vanilla"
PRECISE_LABEL = "Precise aim"
APPROXIMATE_LABEL = "Approximate aim"
dice_data_crossbow = {
    VANILLA_LABEL: [],
    PRECISE_LABEL: [],
    APPROXIMATE_LABEL: []
}
crossbow_diff = {
    VANILLA_LABEL: 0,
    PRECISE_LABEL: -4,
    APPROXIMATE_LABEL: -2
}
for label in [VANILLA_LABEL, PRECISE_LABEL, APPROXIMATE_LABEL]:
    for i in range(1, 7):
        for j in range(1, 7):
            test = CROSSBOW_TEST - i - j + crossbow_diff[label] - 3
            for k in range(1, 7):
                for l in range(1, 7):
                    if test <= 0 or i + j == 12:  # Misses
                        dice_data_crossbow[label].append(0)
                    else:  # Hits
                        # XXX This does not take into account the critical
                        # success
                        damage = get_damage("E", test + k + l)
                        dice_data_crossbow[label].append(damage)

dice_data = sorted(list(dice_data_crossbow.items()))
labels, dice_data = zip(*dice_data)
plot_data(dice_data, label_x="Attack damage", title="crossbow_damage",
          labels=labels)
