import json
import math
import os

import matplotlib.pyplot as plt
import numpy as np

FONTSIZE = 14


color = [
    "orangered",
    "#00B0F0",
    "#949698",
    "red",
    "blue",
    "green",
    "yellow",
    "black",
    "brown",
    "gray",
    "orange",
]


def _one_dot_by_instance(bin_edges, cdf, nbr_instances):
    new_bin_edges = []
    new_cdf = []
    i = 0
    for l in np.linspace(0, 1, num=nbr_instances + 1):
        if i == len(cdf):
            break
        new_bin_edges.append(bin_edges[i])
        new_cdf.append(l)
        if math.fabs(cdf[i] - l) <= 10e-6:
            i += 1
    return new_bin_edges, new_cdf


def _histogram_data(bounded_data):
    counts = []
    bin_edges = []
    bounded_data = sorted(bounded_data)
    for i in range(len(bounded_data)):
        if len(bin_edges) != 0 and bin_edges[-1] == bounded_data[i]:
            counts[-1] += 1
        else:
            counts.append(1)
            bin_edges.append(bounded_data[i])
    return counts, bin_edges


def cdf_data(cdf_values, nbr_instances=None):
    data = sorted(cdf_values)

    # Count and filter math.inf
    bounded_data = []
    for value in data:
        if value != math.inf:
            bounded_data.append(value)
    if len(bounded_data) == 0:  # Every demand file was failed
        return None, None

    counts, bin_edges = _histogram_data(bounded_data)
    cdf = np.cumsum(counts)

    cdf = (cdf / cdf[-1]) * (len(bounded_data) / len(
        data))  # Unsolved instances hurts the cdf
    bin_edges = list(bin_edges)
    bin_edges.insert(0, 0)
    cdf = list(cdf)
    cdf.insert(0, 0)
    if nbr_instances is not None:
        return _one_dot_by_instance(bin_edges, cdf, nbr_instances)
    else:
        return bin_edges, cdf


def plot_data(data, label_x, title, labels=()):
    fig1 = plt.figure()
    subplot = fig1.add_subplot(111)

    # Plot CDFs
    max_value = 0
    min_value = math.inf
    json_data = {}
    for i, d in enumerate(data):
        bin_edges, cdf = cdf_data(d)
        min_value = min(bin_edges[1:] + [min_value])
        max_value = max(bin_edges[1:] + [max_value])
        subplot.step(bin_edges + [max_value * 10 ** 7], cdf + [cdf[-1]],
                     color=color[i],
                     marker="o",
                     linewidth=2.0, where="post", markersize=9,
                     label=labels[i] if len(labels) > i else None)
        json_data[labels[i] if len(labels) > i else i] = {
            bin_edges[j]: cdf[j] for j in range(len(cdf))
        }

    # Graph limits
    if max_value <= min_value:
        xdiff = 0.0
    else:
        xdiff = (max_value - min_value) * 0.1

    xlim_min = min(0.0, min_value - xdiff)
    xlim_max = max_value + xdiff
    if xlim_min != xlim_max:
        subplot.set_xlim(left=xlim_min,
                         right=xlim_max)  # To avoid being too near of 0
    subplot.set_ylim([0, 1])

    # Axes
    subplot.set_xlabel(label_x, fontsize=FONTSIZE)
    subplot.set_ylabel("CDF", fontsize=FONTSIZE)
    subplot.tick_params(axis='both', which='major', labelsize=FONTSIZE, width=2)
    subplot.tick_params(axis='both', which='minor', width=1)

    # Handle labels
    handles, labels = subplot.get_legend_handles_labels()
    if len(handles) != 0 and len(labels) > 1:
        leg = subplot.legend(handles, labels, loc="best", fontsize=FONTSIZE)
        leg.get_frame().set_linewidth(1.5)

    # Save image
    figure_name = title
    output_path = os.path.abspath(os.path.dirname(__file__))
    fig1.savefig(os.path.join(output_path, "%s.cdfs.pdf" % figure_name),
                 bbox_inches='tight', pad_inches=0, markersize=9)
    with open("%s.cdfs.json" % figure_name, "w") as fileobj:
        json.dump(json_data, fileobj, indent=4)
    fig1.clf()
    plt.close()
